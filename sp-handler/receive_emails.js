var open = require('amqplib').connect('amqp://ampcome:Ampcome1@energy.x1platform.com');
var request = require('request');
var { Pool } = require('pg');
var pool = new Pool({
    user: 'postgres',
    host: '172.104.60.89',
    database: 'iotdb',
    port: 6432
});

let ex = 'stream_processor_data';
let q = 'email_alerts';

function start() {
    open.then(function (conn) {
        printLog('Creating channel...');
        return conn.createChannel();
    }).then(function (ch) {
        printLog('Asserting Exchange...');
        ch.assertExchange(ex, 'fanout', { durable: true });
        return ch;
    }).then(function (ch) {
        printLog('Asserting Queue...');
        ch.assertQueue(q, { durable: true });
        return ch;
    }).then(function (ch) {
        ch.bindQueue(q, ex);
        return ch;
    }).then(function (ch) {
        printLog('Consuming Queue...');
        ch.consume(q, function (msg) {

            //Prepare email payload
            let emailJSON = {
                "substitution_data": {},
                "metadata": {
                    "data": [],
                    "meter_name": null
                },
                "options": {
                    "sandbox": false
                },
                "content": {
                    "template_id": "energy-app-alert",
                    "use_draft_template": true
                },
                "recipients": []
            }
            let data = JSON.parse(msg.content.toString());
            for (var i = 0; i < Object.keys(data.event).length; i++) {
                if (Object.keys(data.event)[i] != "emails") {
                    if (Object.keys(data.event)[i] != "meter_id")
                        emailJSON.metadata.data.push({ "key": Object.keys(data.event)[i], "value": Object.values(data.event)[i] });
                }
            }
            let emails = data.event.emails.split(',').map(e => e.trim());
            for (var i = 0; i < emails.length; i++) {
                emailJSON.recipients.push({ "address": { "email": emails[i] } });
            }

            emailJSON.metadata.meter_name = data.event.meter;
            // console.log(JSON.stringify(emailJSON));
            // console.log(JSON.stringify(data));
            console.log('Data received from queue, now processing it !');


            //Send Email
            request.post(
                {
                    url: 'https://api.sparkpost.com/api/v1/transmissions',
                    headers: {
                        'Host': 'api.sparkpost.com',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Accept-Language': 'en-US, en; q=0.5',
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json;charset=utf-8',
                        'Authorization': '1f343d01592931b734e0f65937ff23b7d5074706'
                    },
                    form: JSON.stringify(emailJSON)
                }, function (err, httpResponse, body) {
                    if (err) {
                        console.log('failed');
                        data.event['status'] = 'failed';
                        data.event['alert_server_response'] = body;
                        data.event['alert_type'] = 'email';
                        data.event['alert_stage'] = 'processed';
                        console.log('Err occured while sending alert email, email queued and will be sent again: ', err);
                        insertEvents(data);
                        ch.nack(msg);
                    }
                    else {
                        data.event['status'] = 'succeeded';
                        data.event['alert_server_response'] = body;
                        data.event['alert_type'] = 'email';
                        data.event['alert_stage'] = 'processed';
                        console.log('Message sent and removed from queue !');
                        // console.log(body);
                        insertEvents(data);
                        // ch.ack(msg);
                    }
                });
        });

    }).catch(function (err) {
        setTimeout(function () {
            console.error('Error occured with RabbitMQ: ', err.message);
            console.log('Trying to fix problem !');
            open = null;
            open = require('amqplib').connect('amqp://ampcome:Ampcome1@energy.x1platform.comm');
            start();
        }, 5000);
    });
}


function insertEvents(event_data) {
    if (Object.values(event_data.event)[0] != "DG") {

        pool.connect()
            .then(client => {
                return client.query(`insert into alert_events
                    (date_time,last_updated_ts,meter_id,meter_name,parameter_name,
                    ideal_value,alert_setting,average_of,alert_processing_status,
                    alert_server_response,alert_type,alert_email_recipients,parameter_value) 
                    values('`+ new Date(Object.values(event_data.event)[1]).toISOString() + `',now(),'` + Object.values(event_data.event)[6] + `',
                    '`+ Object.values(event_data.event)[0] + `','` + Object.keys(event_data.event)[3] + `',
                    '`+ Object.values(event_data.event)[4] + `','` + Object.values(event_data.event)[5] + `',
                    '`+ Object.values(event_data.event)[2] + `','` + Object.values(event_data.event)[8] + `',
                    '`+ Object.values(event_data.event)[9] + `','` + Object.values(event_data.event)[10] + `',
                    '`+ Object.values(event_data.event)[7] + `','` + Object.values(event_data.event)[3].replace(/\s\=\>.+/g, '') + `')`)
                    .then(res => {
                        client.release()
                        console.log('Alert Inserted Into Database !');
                    })
                    .catch(e => {
                        client.release()
                        console.log(e);
                        setTimeout(function () { insertEvents(event_data); }, 10000);
                    })
            });
    }
    // pool.end();
}

function printLog(data) {
    console.log(data);
}

start();
