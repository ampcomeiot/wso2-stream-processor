var open = require('amqplib').connect('amqp://ampcome:Ampcome1@energy.x1platform.com');


let ex = 'stream_processor_data';
let q = 'alerts_store';

open.then(function (conn) {
    printLog('Creating channel...');
    return conn.createChannel();
}).then(function (ch) {
    printLog('Asserting Exchange...');
    ch.assertExchange(ex, 'fanout', { durable: true });
    return ch;
}).then(function (ch) {
    printLog('Asserting Queue...');
    ch.assertQueue(q, { durable: true });
    return ch;
}).then(function (ch) {
    ch.bindQueue(q, ex);
    return ch;
}).then(function (ch) {
    printLog('Consuming Queue...');
    ch.consume(q, function (msg) { printLog(msg.content.toString()); ch.ack(msg); });
}).catch(function (err) {
    console.error('Error occured: ', err);
});

function printLog(msg) {
    console.log(msg);
}