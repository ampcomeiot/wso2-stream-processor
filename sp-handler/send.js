var open = require('amqplib').connect('amqp://ampcome:Ampcome1@energy.x1platform.com');


let ex = 'stream_processor_data';


open.then(function (conn) {
    printLog('Creating channel...');
    return conn.createChannel();
}).then(function (ch) {
    printLog('Asserting Exchange...');
    ch.assertExchange(ex, 'fanout', { durable: true });
    return ch;
}).then(function (ch) {
    printLog('Sending msg to Exchange...');
    ch.publish(ex, '', new Buffer(JSON.stringify({ "name": "pkt", "age": 22 })), { persistent: true });
    return ch;
}).then(function (ch) {
    setTimeout(function () {
        ch.close();
        process.exit(1);
    }, 1000)
}).catch(function (err) {
    console.error('Error occured: ', err);
});

function printLog(msg) {
    console.log(msg);
}