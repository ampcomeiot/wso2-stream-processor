const axios = require('axios');
const dns = require('dns');
var open = require('amqplib').connect('amqp://ampcome:Ampcome1@energy.x1platform.com');


function start() {
    let q = 'WSO2_Stream_Processor_Data';
    open.then(function (conn) {
        console.log('Creating channel !');
        return conn.createChannel();
    }).then(function (ch) {
        console.log('Asserting Queue !');
        return ch.assertQueue(q, { durable: true, autoDelete: false }).then(function (ok) {
            console.log('Starting to consume messages !');
            return ch.consume(q, function (msg) {
                console.log('Checking internet connection !');
                dns.resolve('www.google.com', function (err) {
                    if (!err && msg) {
                        console.log('All good, now processing data !');
                        console.log("Received received and ackowledged !");
                        console.log(JSON.parse(msg.content.toString()));
                        // process(JSON.parse(msg.content.toString()));
                        ch.ack(msg);
                    }
                    else {
                        ch.nack(msg);
                    }
                });
            });
        });
    }).catch((err) => {
        setTimeout(function () {
            console.log(err.message);
            open = null;
            open = require('amqplib').connect('amqp://guest:guest@localhost');
            pushData();
        }, 1000);
    });
}

function process() {
    let msg = "Medreich Unit 8, RR No.: NGHT13%0a%0aMeter: HT-VCB%0aPower Factor: 0.894 (89.403%)%0aIdeal Power Factor: 1%0aAlert Setting: 10%";
    console.log('Incoming msg: ', msg);
    axios.post('http://api.msg91.com/api/v2/sendsms',
        {
            "sender": "ENERGY",
            "route": "4",
            "country": "91",
            "sms": [
                {
                    "message": msg,
                    "to": [
                        "8708430892"
                    ]
                }
            ]
        },
        {
            "headers": {
                "authkey": "255529AZw7ShNN3Cs35c3436e0",
                "Content-Type": "application/json"
            }
        }
    ).then((res) => { console.log('Message sent !'); })
        .catch((err) => { console.log('Failed to send sms: ', err.message) });
}


process();